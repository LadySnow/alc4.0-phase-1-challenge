package com.example.alcchallenge;

import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ALCActivity extends AppCompatActivity {
    @BindView(R.id.webView)
    WebView webView;
    String alcurl = "https://andela.com/alc";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutalc);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("About ALC");
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl(alcurl);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError er) {
            // Ignore SSL certificate errors
            handler.proceed();
        }
    }

}
